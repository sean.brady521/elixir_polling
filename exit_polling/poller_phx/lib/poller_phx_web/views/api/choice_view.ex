defmodule PollerPhxWeb.Api.ChoiceView do
  alias PollerPhxWeb.Api.ChoiceView
  use PollerPhxWeb, :view

  alias __MODULE__

  def render("index.json", %{choices: choices}) do
    render_many(choices, ChoiceView, "show.json")
  end

  def render("show.json", %{choice: choice}) do
    %{id: choice.id, name: choice.description}
  end
end
