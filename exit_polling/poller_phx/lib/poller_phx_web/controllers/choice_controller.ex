defmodule PollerPhxWeb.ChoiceController do
  use PollerPhxWeb, :controller

  alias PollerDal.Questions
  alias PollerDal.Questions.Question
  alias PollerDal.Districts
  alias PollerDal.Districts.District
  alias PollerDal.Choices
  alias PollerDal.Choices.Choice

  def index(conn, %{"district_id" => district_id, "question_id" => question_id}) do
    question = Questions.get_question!(question_id)
    district = Districts.get_district!(district_id)
    choices = Choices.list_choices_by_question_id(question_id)
    render(conn, "index.html", question: question, district: district, choices: choices)
  end

  def new(conn, %{"district_id" => district_id, "question_id" => question_id}) do
    question = Questions.get_question!(question_id)
    district = Districts.get_district!(district_id)
    changeset = Choices.change_choice(%Choice{})
    render(conn, "new.html", question: question, district: district, changeset: changeset)
  end

  def create(conn, %{"choice" => choice_params, "district_id" => district_id, "question_id" => question_id}) do
    question = Questions.get_question!(question_id)
    district = Districts.get_district!(district_id)
    choice_params_with_question = Map.merge(choice_params, %{"question_id" => question_id})

    case Choices.create_choice(choice_params_with_question) do
      {:ok, _questions} -> conn
        |> put_flash(:info, "Choice created successfully")
        |> redirect(to: Routes.choice_path(conn, :index, district_id, question_id))
      {:error, %Ecto.Changeset{} = changeset} -> render(conn, "new.html", question: question, district: district, changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id, "district_id" => district_id, "question_id" => question_id}) do
    question = Questions.get_question!(question_id)
    district = Districts.get_district!(district_id)
    choice = Choices.get_choice!(id)
    changeset = Choices.change_choice(choice)
    render(conn, "edit.html", question: question, district: district, choice: choice, changeset: changeset)
  end

  def delete(conn, %{"id" => id, "district_id" => district_id, "question_id" => question_id}) do
    choice = Choices.get_choice!(id)
    {:ok, _choices} = Choices.delete_choice(choice)
    conn
      |> put_flash(:info, "Choice deleted successfully")
      |> redirect(to: Routes.choice_path(conn, :index, district_id, question_id))
  end

  def update(conn, %{"id" => id, "choice" => choice_params, "district_id" => district_id, "question_id" => question_id}) do
    question = Questions.get_question!(question_id)
    district = Districts.get_district!(district_id)
    current_choice = Choices.get_choice!(id)
    case Choices.update_choice(current_choice, choice_params) do
      {:ok, _choices} -> conn
        |> put_flash(:info, "Choice successfully updated")
        |> redirect(to: Routes.choice_path(conn, :index, district_id, question_id))
      {:error, %Ecto.Changeset{} = changeset} -> render(conn, "edit.html", district: district, question: question, choice: current_choice, changeset: changeset)
    end
  end
end
