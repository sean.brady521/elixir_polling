# id, description, choices
defmodule Poller.Choice do
  alias __MODULE__

  defstruct(
    id: nil,
    description: nil,
    party: nil
  )

  def new(id, desc, party) do
    %Choice{
      id: id,
      description: desc,
      party: party
    }
  end
end
