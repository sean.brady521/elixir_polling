width = IO.gets("What is the width? ") |> String.trim
height = IO.gets("What is the height? ") |> String.trim
{n_width, _} = Integer.parse(width)
{n_height, _} = Integer.parse(height)
area = n_width * n_height
IO.puts("The area is #{area}")
