defmodule Party do

  @mascots %{
    democratic: "donkey",
    republican: "elephant",
    libertarian: "statue",
    green: "plant",
    constitution: "eagle",
    whig: "owl"
  }

  @parties Map.keys(@mascots)

  def mascot(party) do
    Map.get(@mascots, party, "other")
  end

  # Default param normal
  def logo(party, size \\ :normal)

  def logo(party, size) when party in @parties do
    party_mascot = mascot(party)
    do_logo(party_mascot, size)
  end

  defp do_logo(mascot, :small), do: "#{mascot}_small.png"
  defp do_logo(mascot, _other_size), do: "#{mascot}_normal.png"
end
