## GenServer
* GenServer.call will send a message and block until it receives a response (syncronous). `.cast` on the other hand will send the message and not wait
* You must create a `handle_cast` function to handle being sent a "cast". Like how we have to create a `handle_call` function
* Cast functions don't send a reply