## Handling errors
* Because all our processing in elixir is managed in small self-contained processes. A process crashing is not a huge deal
* If a process servicing a response to an API request crashes all that means is the request won't get serviced, which it wouldn't even if we managed to not crash
* A long lived process that's managing something important is better to be restarted then trying to manage errors. This way we ensure the process is always in a known good state
* In Elixir we don't program defensively to try and handle the error. We let it crash the process. For long lived processes we need to detect the crash and restart it

## Supervisor processes
* Generally we have a long lived process called a "supervisor process" who's job it is to detect the crashes of other long-lived processes and restart them if they die
* You may also have a supervisor for the short lived processes to handle logging and memory cleanup with a short lived process dies
* In order to prevent linked processes from forming a death pact. The child process needs to call `Process.flag(:trap_exit, true)` to indicate that instead of killing it's parent when dying it should simply notify