## What's a process
* Instead of threads, elixir has "processes"
* Elixir processes are significantly more lightweight than traditional threads. requiring about 2kb of memory instead of 1-3mb
* Processes don't share memory or garbage collectors
* Processes communicate through messages
* Processes can be short or long lived, and it's common to have even a million running at once which would never work with threads

## Messages
* Messages are best though of with a mailbox analogy
* Mesages are sent to a pid with the `send` command
* The recipient then has their mailbox fill up, and can grab messages using the `receive` command
* When a new process is spawned you get the pid returned. You can get the pid of the host process by calling `self()`

## Process life time
* Short lived processes die after a finite period of time
* Long lived porcesses never die and await work
* Processes are great for running short lived work that needs to be parallelised or things you don't want blocking the main thread
* Long lived processes are often used for DB connections and maintaining state

## Linked processes
* You can link up a bunch of processes which basically form a death pact. If one process dies they all die
* This makes sense when you're running a bunch of processes where all of them need to succeed for it to be a success
* `spawn_link` can be used to spawn a function and link it to the parent
* `link(pid)` can be called to link a process to the current process

## Process spawning
* You can spawn a process by passing in an anonymous function like spawn fn -> IO.puts("Beans") 
* You can spawn a module named funciton like `spawn ([module name], [function name as atom], [args])`
  * `spawn(Count, :run, [0])`