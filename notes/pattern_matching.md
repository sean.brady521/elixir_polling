# Pattern matching
* Pattern matching allows us to make assertions around the shape of a value and also destructure it into component parts


# Strings
```
message = "district:42"
"district:" <> district_id = message
```
* district_id = 42
* The above will through an error if district: is not matched


# Maps
```
person = %{"name": "Sean Bean", "age": 52}
%{"name" => "Sean Bean", "age": age} = person
```
* The above will destructure the age number and bind it to our age variable, and it will assert the name is "Sean Bean"

# Lists
`[ head | _ ] = list` - grab the first element from the list
`[ head | tail ] = list` - grab the first element from the list and grab the other elements, equivalent to `[head, ...rest]` in JS
`[a, b] = list` - grab the two elements in the list and assert there are two
`[1, a] = list` - assert the first value is 1, there are two elements and grab the second one and bind it to a

# Tupples
```
message = { :add, [1,2] }
{ var, [ a, b ] } = message
```

* If you want a pattern match that asserts a specific value that comes from a variable you must use the `pin` operator ^
* E.g. 
```
  pid = 123
  {pid, result} -> result
```

will not work, the first tupple will simple be BOUND to pid

```
  pid = 123
  {^pid, result} -> result
```