# Control flow in elixir
* You don't actually use control flow very much in elixir
* There are no loops in elixir as they rely on side effect and mutation

# If statements
* Everything in elixir is an `expression` meaning it returns a value
* Variables are all block scoped

```
message =
  if age < 18 do
    "You can't vote yet"
  else 
    "You can vote!"
  end
```

* The above works because elixir will always treat the last line in a "block" as the return value. So each if block returns the relevant value if the condition is met
* There are no "else if" statements in elixir, use cond instead

# Cond statemetns
* The `cond` expression is essentially a switch that allows boolean logic

```
message = 
  cond do
    age < 18 -> "You can't vote yet"
    age < 25 -> "You can vote!"
    true -> "You can vote and run for office!"
  end
```

* Cond conditions need all cases covers or it will throw an error

# Case expression
* Similar to a switch statement
* Allows pattern matching
* You can add "guards" onto cases which allow you to only match a variable on a boolean condition

basic case expression
```
message =
  case age do
    18 -> "You're 18"
    19 -> "You're older than 18"
    true -> "You're not 18 or 19"
  end
```

case expression with pattern matching and guards
```
  message = 
    case Integer.parse(age) do
      {_age, _} when age < 18 -> "You can't vote"
      {_age, _} when age < 25 -> "You can vote!"
      {_age, _} -> "You can vote and run for office!"
      :error -> "Invalid number"
    end
```

# Unless expression
* Unless is the inverse of if
``` 
message =
  unless age < 18 do
    "You can vote"
  else 
    "you can't vote"
  end
```


# ternary operators
`message = if age < 18, do: "You can vote", else: "You can't vote!"`

* If is a macro and the above is essentially a function call with the first argument as the condition and the second argument as a keyword list

`message = if(age < 18, do: "You can vote", else: "You can't vote!")`

`message = if(age < 18, [do: "You can vote", else: "You can't vote!"])`

# Exceptions
* Exceptions are ok in elixir

