# File Types
* .ex files are set to be compiled into byte code. .exs files are compiled on the fly and not saved
* Use .ex files for production code and .exs files for developer scripting and tests