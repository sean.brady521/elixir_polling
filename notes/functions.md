# Methods vs. functions
* Methods act on their own data and may or may not return a value
* Functions act on input data and return a value based on the input

# Functions in elixir
* The last line of the function is the return value
* Functions in elixir should be very small. Usually 1 line
* Functions in elixir can be duplicated to create multiple "clauses" you can specify the paramter that triggers a given function. See function pattern matching

# Anonymous functions
```
fn age -> if age >= 18, do "You can vote", else "You can't vote"
```

```
add = fn(age)  ->
  if age >= 18, do "You can vote", else "You can't vote"`
end

add.(14)
```


# Named functions
* Named functions must be places in modules

```
def can_vote(age) do
  cond do
    age < 18 -> "You can't vote yet"
    age < 25 -> "You can vote!"
    true -> "You can vote and run for office!"
  end
end
```

# Function pattern matching
* You can add guards to functions as well to help define clauses
* The ordering of clauses matters. Each clause is considered from top to bottom
* Clauses must be contiguous
* Functions are usually so short in elixir because generally you'll use the below pattern

```
  defmodule Voter do
    def eligibility(age) when is_binary(age) do
      eligibility(Integrate.parse(age))
    end
    
    def eligibility({age, _}) do
      eligibility(age)
    end

    def eligibility(age) when is_integrate(age) and age < 18 do
      "You can't vote"
    end

    def eligibility(age) when is_integrate(age) and age < 25 do
      "You can vote!"
    end

    def eligibility(:error) do
      "Invalid Number!"
    end

    def eligibility (_age) do 
      "You can vote and run for office!"
    end
```

# Module attributes
* You can add module attributes to a module using the @ symbol
* One such attribute is "moduledocs" which lets you define the documentation that pops up when you use the `h` function
* See `src/function_docs.exs`
* When you format the docs like the above they also act as tests. The system automatically checks that your documentation is correct essentially

# Private functions
* Private functions are defined by `defp`
* Private functions are important as they prevent bugs. You don't want to expose all the clauses of your functions as it might lead to unexpected inputs
* Only expose two clauses. The case where the input is in a valid shape you expect and the catch all error case. See `src/function_docs.exs`

# Anonymous function pattern matching
* Anonymous functions can have multiple params and body lines to fascilitate pattern matching

```
  logo_file = fn
    (:democrat) -> "donkey.png"
    (:republican) -> "elephant.png"
    (:green) -> "flower.png"
    _ -> "missing.png"
```

# LISP 2 namespaces
* Elixir uses LISP 2 namespaces which means that functions and variables are namespaced differently
* I.e. I can define a function called "add" and a variable called "add" and there won't be any conflicts. 
* When I call the function add elixir will go to the function namespace to check for add and there won't be a conflict
* If I use anonymous functions assigned to variables though then we end up in the variable namespace
* Adding a `.` after the function call tells elixir to go the the variable namespace to grab the function
* e.g. `add.(1)` 

# First class functions
* When you're able to pass around functions like values (i.e. pass them as params or return them) then they are considered "first class functions"
* Higher order functions are functions that received a function as a param or return a function
* Anonymous functions are able to utilise closures but named functions are not
* You can not pass named functions around as params because elixir is expecting the param functions in the variable namespace
  * You can convert a named function to an anonymous (variable namespaced) function by using & like `&Module.named_func/1`. You must also specify the arity (number of params)
* See `src/higher_order_named_functions.exs`

# Guards
* Only certain functions in the Kernel module can be used as guards to ensure performance is ok
  * You can look at the help docs for a function in the kernel module and it will say if it can be used as a guard or not

* If you call a function as part of a guard and the function fails then the guard will simply swollow the error and the function will not run. So be careful

# Sigils 
* Sigils are basically shorthand ways of manipulating data
* E.g.
  * `~w/Will Kristen Tina` - splits the string by whitespace
  * `~r/will` - created a regex
* You can even make your own sigils

# Piping
* When pipeing. The piped value is always passed in as the FIRST input parameter. Meaning you can specify other parameters assuming the piped value will be slotted in at the start. E.g. `"Elixir-is-cool" |> String.split("-")`

# Recursion
* You'll have to use recursion a lot in elixir as there are no loops
* Recursion can have a lot of stack memory issues. So to get around this elixir has "Tail call optimisation"
* Ensure that the last call to your recursive function is simply a function call and this will activate

# Misc
* You can define constants as module attributes. See `src/odds_and_ends.exs`
* You can add a default parameter by adding an additional function clause with `\\` and no body. See above file