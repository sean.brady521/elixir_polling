# Integers
* You can add _ into numbers to make them more readable and elixir will strip them out. E.g. 100_000_000
* Dividing a number turns it into a float
* There's no maximum integer size in elixir

# Atoms
* constant where the value is the name. E.g. `:ok` is the same as `ok = "ok"`
* Atoms use up non-recoverable memory. Use atoms in code but never let them be created by user input

# Booleans
* When you use and chainng and have a non boolean at the end that will be returned. E.g. `true and 1` will return 1. All but the rightmost value must be a boolean.
* `not` will flip a boolean  

# Strings
* double quotes creates a string, single quotes creates a char list
* When passing a string to an erlang function it actually requires a "char list". This can be created using single quotes or a transformation function
* Erlang = '
* Elixir = "
* Concat strings with `<>`
* String interpolation with `#{[variable]}`. E.g. "Hello #{name}"

# Maps
* Map syntax `%{key => value}`
* Keys and values can be any types
* Maps are not mutable. Update a value with this syntax `%{[myMap] | [updateKey] => [newVal]}`. This will return the updated map but not modify the existing one
  * This function only works for updating existing keys
* If you use atoms as the keys you can use the following syntactic sugar
```
  variable = %{
    will: 0,
    kristen: 0,
    tina: 0
  }
```
* And you can access with `variable.will` (atom keyed only)

# Lists
* Lists in Elixir are singly linked lists
* Prefer prepending elements as it's a cheap list operation
* Consider the performance implications of a linked list before using this data structure
* You can prepend things with `[[new item] | [arr]]` (this doesn't work for appending)
* A lot of values are stored by reference across variables in elixir because values are immutable so there's no risk of mutation
* E.g.
```
counts = [5, 8, 6]
new_counts = [3 | counts]
```
* The values 5, 8, 6 in both lists use the same spot in memory. This would be dangerous if mutation was a thing
* You can append lists with `arr + [vals]`
* Remove element from the list with `arr -- [vals]`

# Key-value pair
* This is a list of key-value pair tupples
* `[{:first_key, "First value"}, {:second_key, "Second value"}]`
  * Shorthand: `[first_key: "First value", second_key: "Second value"]`
  * Drop the curlies and move the colon to the front
* It's basically a way of having a list with keys associated with them instead of just values. 
* Often in elixir when a function has a lot of optional parameters it will have one `opts` parameter which take a key value list of the params you want.
* E.g. `IO.inspect(person, [{:label, "Step 1"}])`
  * Or with shorthand `IO.inspect(person, [label: "Step 1"])`
* You can have duplicate keys in a key-value list. Getting it will provide the first one

# Structs
* Structs let us define a map with a specific set of keys
* For syntax see module definition below. These must be created as a module
* Syntax to initliase `my_candidate = %Candidate{name: "Bean", age: 22, party: :democrat}`
* All non-specified values that are required will be set to `nil`

# Macros
* Macros are like functions that receive code as parameters, transform it and return new code
* For example `defmodule` is a macro. When you enter
```
defmodule Candidate do
  defstruct [:name, :age, :party]
end
```
* What you're really doing is providing the defmodule macro with a bunch of code that it's going to use to generate a module

# Variables
* reassignment of variable names is allowed in elixir and will never mutate data, you'll just lose you reference to the old piece of data
* The = sign in elixir is more of an equality operator than an assignment. You're not "assigning" variables you're declaring equivalence 
* it's called the "match" operator. Instead of assigning we "bind"

# Misc
* type `i([data])` to get information on that data
* Use tab completion in iex to see module functions
* Get documentation on a function by typing `h([function])`
* Elixir will allow you to do comparison operators across data types
* number < atom < reference < function < port < pid < tuple < list < bitstring 