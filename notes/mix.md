## Use macro
* the use macro lets you extend the functionality of another module. Sort of like `extends`
* It calls the `__using__` method on the module in question which does some stuff to help another module extend it

## Tests
* Test files must end in _test.exs
* Doctest runs all of the documentation tests

## Functional programming
* Functional programming is about maximising the use of pure functions and minimising side effects and mutations
* Where side effects are absolutely necessary we ensure they're tightly confined and controlled. Treating them as radioactive material
* Most bugs are caused by side effects
* When working on a functional programming project, a good order of operations is:
1. Create your project
2. Consider what data structures will be required and create them
3. Create the functions that will output the data into the view
4. Create the functions that will update the data
5. Create the control code that will manage the state/side effects

## Running in iex
* You can run a mix project in iex by running `iex -S mix`

## IO Lists
* In elixir instead of concatenating strings and lists you can just output lists, even nested lists and the IO functions will handle converting them to strings