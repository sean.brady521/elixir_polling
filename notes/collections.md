# Eager vs. lazy functions
* Eager functions take enumerables as input, do all the work they can over the whole data set and return the value
* Lazy functions only do work when they are chained to an Eager function.
* Streams are lazy, Enum functions are eager
* Streams are used to create more memory efficient pipelines
* Lazy functions collect a list of all the lazy functions they're chained two. Then when processing an enumerable they iterate over each element one by one and apply all the functions. 
* Eager functions run the whole data set over each function, creating intermediary data.

```
def lazy_sum(nums) do 
  nums
  |> Steam.map(fn n -> n * 2 end) // Lazy
  |> Steam.filter(&is_even/1) // Lazy
  |> Enum.sum() // Eager

```

# Comprehensions