## Behaviours
* Behaviours are like a contract saying your module will behave a certain way (implement some particular functions)
* It's a bit like implementing an abstract class
* Type `b(behaviour_name)` to see the required methods

## Applications
* Making your module implement the "application" behavaiours let's the module auto start. Like defining `main()` or `__init__()`
* Add `mod: {App, []}` to your application mix.exs