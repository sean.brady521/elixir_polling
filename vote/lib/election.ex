defmodule Election do
  defstruct(
    name: "",
    candidates: [],
    next_id: 1
  )

  def run() do
    run(%Election{})
  end

  def run(:quit) do
    :quit
  end

  def run(election) do
    [ IO.ANSI.clear(), IO.ANSI.cursor(0, 0) ] |> IO.write()

    election
      |> view
      |> IO.write()

    command = IO.gets(">")

    updated = Election.update(election, command)
    run(updated)
  end

  @doc """
  Updates Election Struct, based on provided command.
  ## Parameters
    - election: Election Struct
    - cmd: String based command. Each command can be shortened to what's shown
      in parenthesis.
      - (n)ame command updates the election name
        - example: "n Mayor"
      - (a)dd command adds a new candidate
        - example: "a Will Ferrell"
      - (v)ote command increments the vote count for candidate
        - example: "v 1"
      - (q)uit command returns a quit atom
        - example: "q"
  Returns `Election` struct
  ## Examples
      iex> %Election{} |> Election.update("n Mayor")
      %Election{name: "Mayor"}

  """
  def update(election, cmd) when is_binary(cmd) do
    update(election, String.split(cmd))
  end

  def update(election, ["n" <> _ | name_parts]) do
    name = Enum.join(name_parts, " ")
    Map.put(election, :name, name)
  end

  def update(_election, ["q" <> _]) do
    :quit
  end

  def update(election, ["v" <> _, id]) do
    vote(election, Integer.parse(id))
  end

  def update(election, ["a" <> _ | name_parts]) do
    name = Enum.join(name_parts, " ")
    election
    |> Map.put(:candidates, [Candidate.new(election.next_id, name) | election.candidates])
    |> Map.put(:next_id, election.next_id + 1)
  end

  def update(_election, _command) do
    :error
  end

  defp vote(election, {id, ""}) do
    updated_candidates = Enum.map(
      election.candidates,
      fn can -> if can.id === id, do: increment_vote(can), else: can end
    )
    Map.put(election, :candidates, updated_candidates)
  end

  defp vote(election, :error), do: election

  defp increment_vote (candidate) do
    Map.put(candidate, :votes, candidate.votes + 1)
  end

  def view_header(election) do
    [
      "Election for: #{election.name}\n"
    ]
  end

  def view_body(election) do
    election.candidates
    |> sort_candidates()
    |> candidate_to_row()
    |> prepend_table_header()
  end

  def view_footer() do
    [
      "\n",
      "commands: (n)ame <election>, (a)dd <candidate>, (v)ote <id>, (q)uit\n"
    ]
  end

  def view(election) do
    [
      view_header(election),
      view_body(election),
      view_footer()
    ]
  end

  defp sort_candidates(candidates) do
    candidates
    |> Enum.sort(&(&1.votes > &2.votes))
  end

  defp candidate_to_row(candidates) do
    candidates
    |> Enum.map(fn %{name: name, votes: votes, id: id} ->
      [
        "#{id}\t#{name}\t#{votes}\n"
      ]
    end)
  end

  defp prepend_table_header(candidates) do
    [
      "ID\tname\tvotes\n",
      "----------------------------------------\n"
      | candidates
    ]
  end
end
