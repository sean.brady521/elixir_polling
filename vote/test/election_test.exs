defmodule ElectionTest do
  use ExUnit.Case
  doctest Election

  setup do
    %{election: %Election{}}
  end

  test "updating election name from command", ctx do
    command = "name Will Ferrel"
    election = Election.update(ctx.election, command)
    assert election == %Election{name: "Will Ferrel"}
  end

  test "adding a new candidate from a command", ctx do
    command = "add Bennie Boi"
    election = Election.update(ctx.election, command)
    expected = %Election{
      candidates: [
        %Candidate{
          id: 1,
          name: "Bennie Boi",
          votes: 0
        },
      ],
      name: "",
      next_id: 2
    }
    assert election == expected
  end

  test "voting for a candidate from a command", ctx do
    command = "vote 1"
    initElection = %Election{
      candidates: [
        %Candidate{
          id: 1,
          name: "Bennie Boi",
          votes: 0
        },
      ],
      next_id: 2
    }

    expected = %Election{
      candidates: [
        %Candidate{
          id: 1,
          name: "Bennie Boi",
          votes: 1
        },
      ],
      name: "",
      next_id: 2
    }

    assert Election.update(initElection, command) == expected

  end

  test "invalid command", ctx do
    command = "beans"
    assert Election.update(ctx.election, command) == :error
  end

  test "quitting the app", ctx do
    command = "quit"
    assert Election.update(ctx.election, command) == :quit

  end
end
