defmodule HelloWeb.OtherController do
  use HelloWeb, :controller

  def index(conn, %{ "myVar" => myVar }) do
    render(conn, "index.html", myVar: myVar)
  end
end
