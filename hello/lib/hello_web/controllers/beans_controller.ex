defmodule HelloWeb.BeansController do
  use HelloWeb, :controller

  def index(conn, %{ "myVar" => myVar }) do
    render(conn, "index.html", myVar: myVar)
  end
end
