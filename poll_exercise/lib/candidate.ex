defmodule Candidate do
  defstruct [:name, :votes]

  def new(name, votes \\ 0) do
    %Candidate{name: name, votes: votes}
  end

  def vote(candidate) do
    Map.put(candidate, :votes, candidate.votes + 1)
  end
end
