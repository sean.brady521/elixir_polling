defmodule Poll do
  defstruct candidates: []

  def new(candidates \\ []) do
    %Poll{candidates: candidates}
  end

  def start_link() do
    spawn_link(Poll, :run, [Poll.new()])
  end

  def add_candidate(pid, candidate) do
    send(pid, { :add, candidate })
  end

  def candidates(pid) do
    send(pid, {:candidates, self()})
    receive do
      {candidates, ^pid} -> candidates
    after
      5_000 -> nil
    end
  end

  def vote(pid, candidate) do
    send(pid, { :vote, candidate })
  end

  def exit(pid) do
    send(pid, :exit)
  end

  # TODO: Implement functions neccesary to make the tests in
  # `test/poll_test.exs` pass. More info in README.md

  def run (:exit) do
    :exit
  end

  def run (poll) do
    receive do
      message ->
        Poll.handle(message, poll)
    end
    |> IO.inspect()
    |> run()
  end

  def handle({:add, candidate}, poll) when is_bitstring(candidate) do
    Map.put(poll, :candidates, [Candidate.new(candidate) | poll.candidates])
  end

  def handle({ :candidates, pid }, poll) do
    send(pid, { poll.candidates, self() })
    poll
  end

  def handle({ :vote, candidate }, poll) when is_bitstring(candidate) do
    new_candidates = Enum.map(poll.candidates, fn can -> if can.name === candidate, do: Candidate.vote(can), else: can end)
    Map.put(poll, :candidates, new_candidates)
  end

  def handle(:exit, _poll) do
    :exit
  end
end
